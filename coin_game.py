from random import choice
from collections import Counter, namedtuple
from pprint import pformat, pprint

def simulate():
    prev = -1
    while True:
        curr = choice((0, 1))
        yield curr
        if prev == curr == 0:
            break
        prev = curr

def simulate_verbose():
    t = tuple(simulate())
    print( f"Outcome: {sum(t):2d}  Sequence: {t}" )
    return t


def simulate_game(n, verbose=False):
    do_sim = verbose and simulate_verbose or simulate
    s = 0
    c = Counter()
    for i in range(n):
        res = sum(do_sim())
        s += res
        c.update([res])
    return {
                "n_sims": n, 
                "avg_win": s / n, 
                "distribution": dict(c)
           }

if __name__ == "__main__":
    from optparse import OptionParser
    
    optparser = OptionParser()
    optparser.add_option("-n", "--n_games", dest="n_games", default=1000, 
                         help="number of games in the simulation",
                         )
    optparser.add_option("-v", "--verbose", dest="verbose", action="store_true",
                         default=False, help="verbose (display individual games)"
                        )
    options, args = optparser.parse_args()

    pprint(simulate_game(int(options.n_games), options.verbose))

